interface Date {
  daysSince: (Date) => number;
  calendarDaysSince: (Date) => number;
  hoursSince: (Date) => number;
  isSameDate: (Date) => boolean;
  isToday: (Date) => boolean;
  isTomorrow: (Date) => boolean;
  isSunday: (Date) => boolean;
  isSaturday: (Date) => boolean;
  isWeekend: (Date) => boolean;

}

Date.prototype.daysSince = function (another: Date) {
	const ms_in_a_day = 86400000
	return (this.getTime() - another.getTime()) / ms_in_a_day
};

Date.prototype.calendarDaysSince = function (another: Date) {

  //Set everything to 0 to get only number of calendar days difference
  var dateTo = new Date(another)
	dateTo.setMilliseconds(0)
	dateTo.setSeconds(0)
	dateTo.setMinutes(0)
	dateTo.setHours(0)
	var dateFrom = new Date(this)
	dateFrom.setMilliseconds(0)
	dateFrom.setSeconds(0)
	dateFrom.setMinutes(0)
	dateFrom.setHours(0)
	
  return dateFrom.daysSince(dateTo)
};

 Date.prototype.isToday = function (){
	var today = new Date();
	return this.isSameDate(today);
}

Date.prototype.isTomorrow = function (){
	var tomorrow = new Date();
	tomorrow.setDate(tomorrow.getDate()+1);
	return this.isSameDate(tomorrow);
}

Date.prototype.isSameDate = function (date: Date){
	return this.getFullYear()==date.getFullYear() && this.getMonth()==date.getMonth() && this.getDate()==date.getDate();
}

Date.prototype.hoursSince = function (dateFrom: Date){

	var date_hasta = new Date(this);
	var date_desde = new Date(dateFrom);


	var ms_in_an_hour = 3600000;
	var days = (date_hasta.getTime() - date_desde.getTime()) / ms_in_an_hour;
	
	return days;
}

Date.prototype.isSunday = function() {
	return this.getDay() === 0;
}

Date.prototype.isSaturday = function() {
	return this.getDay() === 6;
}

Date.prototype.isWeekend = function() {
	return this.isSaturday() || this.isSunday();
}