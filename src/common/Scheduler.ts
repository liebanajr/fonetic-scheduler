import { log } from '../logging/log'
import { RecognitionResponse } from '../recognizers/fonetic-recognizer'
import './extensions'

export class Scheduler {

	schedule: Appointment[]
	filteredSchedule?: Appointment[]
	offeredAppointments?: Appointment[]
	selectedAppointment?: Appointment

	parseDateFromLuisRecognizer = function(response: RecognitionResponse) : Date{
		let dateString = response.top_result.result
		let day = dateString.substr(0,2)
		let month = dateString.substr(3,2)
		let year = dateString.substr(6,4)

		let formattedDate = `${year}-${month}-${day}`
		let date = new Date(formattedDate)
		log.debug(`Response=${JSON.stringify(response)} | formattedDate=${formattedDate} | date=${date}`)
		if(date) {
			return date
		}
	}

	/**
 * Hora a partir de la cual se considera horario de tarde, en formato 24 horas. Ej. 15 si es desde las 15:00
 */
	private readonly MORNING_THRESHOLD = 15;
	private readonly DIAS_SEMANA = ["domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado"];

	/**
	 * Obtiene las citas de la agenda a filtrar que se encuentran entre hoy y maxDays en el futuro
	 */
	filterScheduleToMaxDaysInTHeFuture = function (schedule: Appointment[], maxDays: number) : Appointment[] {
		var today = new Date();
		var filteredAppointments: Appointment[]

		for (const appointment of schedule) {
			var date = appointment.startDate;
			if (date.daysSince(today) <= maxDays && date > today) {
				filteredAppointments.push(appointment);
			}
		}
		return filteredAppointments

	}

	filterScheduleIfMorning = function (schedule: Appointment[], isMorning: boolean) {

		var filteredAppointments : Appointment[]
		for (const appointment of schedule) {
			var date = appointment.startDate
			var time = date.getHours();
			if ((isMorning && time <= this.MORNING_THRESHOLD) ||
				(!isMorning && time > this.MORNING_THRESHOLD)) {
				filteredAppointments.push(appointment);
			}
		}
		return filteredAppointments;

	};

	/**
	 *  Obtiene un subgrupo de citas en dias diferentes (se queda con solo con la primera cita disponible de cada día)
	 */
	filterScheduleInDifferentDays = function (schedule: Appointment[]) {

		var filteredSchedule = [];

		//Nombramos bucle para poder salir desde un subbucle
		loop:
		for (const appointment of schedule) {
			var date = appointment.startDate;

			for (const filteredAppointment of filteredSchedule) {
				var filteredDate = filteredAppointment.startDate;
				//Si ya tengo en 'citas' otra cita para el mismo día, no la añado, y salto a mirar la siguiente cita
				if (filteredDate.isSameDate(date)) {
					continue loop;
				}
			}
			filteredSchedule.push(appointment);
		}
		return filteredSchedule;

	};

	hasCitasDeManana = function (schedule: Appointment[]) {
		return this.filtraAgendaSiEsMananaOTarde(schedule, true).length > 0
	};

	hasCitasDeTarde = function (schedule: Appointment[]) {
		return this.filtraAgendaSiEsMananaOTarde(schedule, false).length > 0
	};

	hasCitasDeMananaYTarde = function (schedule: Appointment[]) {
		return this.hasCitasDeManana(schedule) && this.hasCitasDeTarde(schedule);
	};

	hasCitasSoloDeManana = function (schedule: Appointment[]) {
		return this.hasCitasDeManana(schedule) && !this.hasCitasDeTarde(schedule);
	};

	hasCitasSoloDeTarde = function (schedule: Appointment[]) {
		return !this.hasCitasDeManana(schedule) && this.hasCitasDeTarde(schedule);
	};

	/**
	 * Elimina de this.agendaFiltrada las citas que haya en el array citasAEliminar.
	 * @param {*} appointmentsToRemove con el array de citas que se quieren eliminar de this.agendaFiltrada
	 */
	removeAppointmentsFromSchedule = function (schedule: Appointment[], appointmentsToRemove: Appointment[]) {
		var remainingAppointments: Appointment[]

		for (var i = 0; i < schedule.length; i++) {
			var citaAgenda = schedule[i];
			var existeCitaEnAgendaFiltrada = false;
			for (var j = 0; j < appointmentsToRemove.length; j++) {
				var citaAEliminar = appointmentsToRemove[j];
				if (citaAgenda.equals(citaAEliminar)) {
					existeCitaEnAgendaFiltrada = true;
					break;
				}
			}
			//Si la cita no está en el array a eliminar, la mantentemos poniéndola en el array nuevo con las restantes
			if (!existeCitaEnAgendaFiltrada) {
				remainingAppointments.push(citaAgenda);
			}
		}
		return remainingAppointments;
	}


	/**
	 * Comprueba los slots devueltos por la gramática del menú varias citas con las citas ofrecidas para devolver:
	 * -'ninguno'
	 * -'repetir'
	 * -cita seleccionada (pudiéndose haber seleccionado por 'hoy','manana','primera','segunda','tercera',el día de la semana (lunes-domingo), o si no es por nada de eso, por el día del mes (del 1 al 31) 
	 * -null si no hay coincidencias entre lo dicho por el usuario y las citas ofrecidas
	 */
	extractSelectedAppointment = function (fromAppointments: Appointment[], withSelectionSlot: any) : Appointment | string | null {

		var day = withSelectionSlot.slotOptionDAY ? withSelectionSlot.slotOptionDAY : ''; //Valores posibles: lunes-domingo,repetir,ninguno,primera,segunda,tercera,hoy,manana
		var diaMes = withSelectionSlot.slotOptionNUM ? Number(withSelectionSlot.slotOptionNUM) : null;

		var diaSemana;
		switch (day) {
			case '':
				break;
			case 'hoy':
				var fechaSeleccionada = new Date();
				return this.findCitaConFecha(fromAppointments, fechaSeleccionada);
			case 'manana':
				var fechaSeleccionada = new Date();
				fechaSeleccionada.setDate(fechaSeleccionada.getDate() + 1);
				return this.findCitaConFecha(fromAppointments, fechaSeleccionada);
			case 'primera':
				return fromAppointments[0];
			case 'segunda':
				return fromAppointments[1];
			case 'tercera':
				return fromAppointments[2];
			case 'ninguno':
				return 'ninguno';
			case 'repetir':
				return 'repetir';
			default:
				diaSemana = day; //lunes, martes, miércoles, jueves, viernes, sábado o domingo
		}
		//Primero priorizamos la selección por el día de la semana
		if (diaSemana) {
			var diaSemanaNumber = this.DIAS_SEMANA.indexOf(diaSemana);
			var citas = this.findCitasConDiaSemana(fromAppointments, diaSemanaNumber);
			return citas ? citas[0] : null;

		}
		//Si no devolvió nada en slot diaSemana, miramos del de diaMes
		else if (diaMes) {
			var citas = this.findCitasConDiaMes(fromAppointments, diaMes);
			return citas ? citas[0] : null;
		}
		else {
			return null;
		}

	};

	findCitaConFecha = function (schedule: Appointment[], date: Date) {

		var fechaDiaSemana = this.DIAS_SEMANA[date.getDay()];
		var fechaDiaMes = date.getDate();
		for (var i = 0; i < schedule.length; i++) {
			var cita = schedule[i];
			var citaDiaSemana = this.DIAS_SEMANA[cita.startDate.getDay()];
			var citaDiaMes = cita.startDate.getDate();
			if (fechaDiaMes == citaDiaMes && fechaDiaSemana == citaDiaSemana) {
				return cita;
			}
		}
	};

	findCitasConDiaSemana = function (schedule: Appointment[], weekDay: number) {
		var citas : Appointment[];
		for (var i = 0; i < schedule.length; i++) {
			var cita = schedule[i];
			var citaDiaSemana = cita.startDate.getDay();
			if (weekDay == citaDiaSemana) {
				citas.push(cita);
			}
		}
		return citas;
	};

	findCitasConDiaMes = function (schedule: Appointment[], monthDay: number) {
		var citas : Appointment[];
		for (var i = 0; i < schedule.length; i++) {
			var cita = schedule[i];
			var citaDiaMes = cita.startDate.getDate();
			if (monthDay == citaDiaMes) {
				citas.push(cita);
			}
		}
		return citas;
	};

	findCitaConDiaSemanaYMes = function (schedule: Appointment[], diaSemanaNumber: number, diaMes: number) {
		for (var i = 0; i < schedule.length; i++) {
			var cita = schedule[i];
			var citaDiaSemana = cita.startDate.getDay();
			var citaDiaMes = cita.startDate.getDate();
			if (diaMes == citaDiaMes && diaSemanaNumber == citaDiaSemana) {
				return cita;
			}
		}
	};

}


export class Appointment {

	appointmentId: string
	startDate: Date

	constructor(id: string, startDate: Date) {
		this.appointmentId = id
		this.startDate = startDate
	}
	equals = function (another: Appointment) {
		return this.appointmentId == another.appointmentId && this.startDate == another.startDate
	}
}