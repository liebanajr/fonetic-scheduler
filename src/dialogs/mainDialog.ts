// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

import { InputHints, MessageFactory, StatePropertyAccessor, TurnContext } from 'botbuilder';

import {
    ComponentDialog,
    DialogSet,
    DialogState,
    DialogTurnResult,
    DialogTurnStatus,
    TextPrompt,
    WaterfallDialog,
    WaterfallStepContext
} from 'botbuilder-dialogs';
import { Scheduler } from '../common/Scheduler';
import { log } from '../logging/log';
import { FoneticRecognizer } from '../recognizers/fonetic-recognizer';

const CITA_INTRO = 'cita.intro';

export class MainDialog extends ComponentDialog {

    private foneticRecognizer : FoneticRecognizer

    constructor(dialogId: string, conversationData: StatePropertyAccessor<DialogState>, foneticRecognizer: FoneticRecognizer) {
        super(dialogId);

        this.foneticRecognizer = foneticRecognizer


        // Define the main dialog and its related components.
        this.addDialog(new WaterfallDialog(CITA_INTRO, [
            this.intro.bind(this),
            this.askFecha.bind(this),
            this.checkFecha.bind(this)
        ]))
        .addDialog(new TextPrompt('askFecha'))

        this.initialDialogId = CITA_INTRO;
    }

    /**
     * The run method handles the incoming activity (in the form of a DialogContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {TurnContext} context
     */
    public async run(context: TurnContext, accessor: StatePropertyAccessor<DialogState>) {
        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);

        const dialogContext = await dialogSet.createContext(context);
        const results = await dialogContext.continueDialog();
        if (results.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id); //Second parameter is accesible in stepContext.options
        }
    }

    private async intro(stepContext: WaterfallStepContext): Promise<DialogTurnResult> {
        const messageText = `¡Hola! A continuación, vamos a concertar su cita médica.`
        const promptMessage = MessageFactory.text(messageText, messageText)
        await stepContext.context.sendActivity(messageText)
        return await stepContext.next()
    }

    private async askFecha(stepContext: WaterfallStepContext): Promise<DialogTurnResult> {
        log.debug(`Result from last step: ${stepContext.result}`)
        const messageText = `¿A partir de qué fecha necesita su cita?`
        const promptMessage = MessageFactory.text(messageText, messageText)
        return await stepContext.prompt("askFecha",messageText)
    }

    private async checkFecha(stepContext: WaterfallStepContext): Promise<DialogTurnResult> {
        log.debug(`Result from last step: ${stepContext.result}`)
        let result = await this.foneticRecognizer.recognize(stepContext.result, "date", "fonetic-scheduler", "emulator")
        let scheduler = new Scheduler()
        let date = scheduler.parseDateFromLuisRecognizer(result)
        log.debug(`Received date: ${JSON.stringify(date)}`)
        const messageText = `Gracias, su cita ha quedado registrada para el ${date}`
        const promptMessage = MessageFactory.text(messageText, messageText)
        await stepContext.context.sendActivity(messageText)
        return await stepContext.endDialog();
    }
}
