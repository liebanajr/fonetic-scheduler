// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

import { ActivityHandler, BotState, ConversationState, StatePropertyAccessor, UserState } from 'botbuilder';
import { Dialog, DialogSet, DialogState } from 'botbuilder-dialogs';
import { MainDialog } from './dialogs/mainDialog';
import { log } from './logging/log';
import { FoneticRecognizer } from './recognizers/fonetic-recognizer';

export class SchedulerBot extends ActivityHandler {
    private conversationState: BotState;
    private dialog: Dialog;
    private dialogState: StatePropertyAccessor<DialogState>;

    constructor(conversationState: BotState, foneticRecognizer: FoneticRecognizer) {
        super();
        if (!conversationState) {
            throw new Error('[DialogBot]: Missing parameter. conversationState is required');
        }

        let dialogState = conversationState.createProperty<DialogState>('DialogState') as StatePropertyAccessor<DialogState>;

        this.dialog = new MainDialog('mainDialog', dialogState, foneticRecognizer)

        this.conversationState = conversationState as ConversationState;
        this.dialogState = this.conversationState.createProperty<DialogState>('DialogState');

        this.onTurn(async (context, next) => {
            await next()
        })
        this.onMessage(async (context, next) => {
            // Run the Dialog with the new message Activity.
            await (this.dialog as MainDialog).run(context, this.dialogState);

            // By calling next() you ensure that the next BotHandler is run.
            await next();
        });

        this.onMembersAdded(async (context, next) => {
            await (this.dialog as MainDialog).run(context, this.dialogState)
            // By calling next() you ensure that the next BotHandler is run.
            await next();
        });
    }
}
