require('dotenv').config()

export const config = {
  app: {
    port: parseInt(process.env.PORT),
    loglevel: process.env.LOGLEVEL,
    luisRecognizer: {
      applicationIdGenerali: process.env.LUIS_RECOGNIZER_GENERALI_APP_ID,
      applicationIdOC: process.env.LUIS_RECOGNIZER_OC_APP_ID,
      endpointKey: process.env.LUIS_RECOGNIZER_ENDPOINTKEY,
      endpoint: process.env.LUIS_RECOGNIZER_ENDPOINT,
      environment: process.env.LUIS_RECOGNIZER_ENVIRONMENT == "true"
    },
    foneticRecognizer: {
      endpoint: process.env.FONETIC_RECOGNIZER_ENDPOINT
    },
    fit: {
      endpoint: process.env.FIT_ENDPOINT
    },
    storage: {
      "type": process.env.STORAGE_TYPE, // memory / mongo
      "url": process.env.STORAGE_URL,
      "database": process.env.STORAGE_DATABASE,
      "collection": process.env.STORAGE_COLLECTION
    }
  }
}