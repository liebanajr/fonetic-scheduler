import * as path from 'path';
import * as express from 'express';
import { config } from './config/config';
import { log } from './logging/log'

import { AutoSaveStateMiddleware, BotFrameworkAdapter, ConversationState, MemoryStorage, StatePropertyAccessor } from 'botbuilder';

// This bot's main dialog.
import { DialogSet, DialogState } from 'botbuilder-dialogs';
import { SchedulerBot } from './bot';
import { FoneticRecognizer } from './recognizers/fonetic-recognizer';


// Create HTTP server.
const server = express()
server.listen(config.app.port, () => {
    log.info(`Server listening on port ${config.app.port}`)
});

// Create adapter.
// See https://aka.ms/about-bot-adapter to learn more about adapters.
const adapter = new BotFrameworkAdapter({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword
});

// Catch-all for errors.
const onTurnErrorHandler = async (context, error) => {
    // This check writes out errors to console log .vs. app insights.
    // NOTE: In production environment, you should consider logging this to Azure
    //       application insights.
    log.error(`\n [onTurnError] unhandled error: ${ error }`);

    // Send a trace activity, which will be displayed in Bot Framework Emulator
    await context.sendTraceActivity(
        'OnTurnError Trace',
        `${ error }`,
        'https://www.botframework.com/schemas/error',
        'TurnError'
    );

    // Send a message to the user
    await context.sendActivity('The bot encountered an error or bug.');
    await context.sendActivity('To continue to run this bot, please fix the bot source code.');
};

// Set the onTurnError for the singleton BotFrameworkAdapter.
adapter.onTurnError = onTurnErrorHandler;

//Bot state management
let conversationState: ConversationState;
const memoryStorage = new MemoryStorage();
conversationState = new ConversationState(memoryStorage);
adapter.use(new AutoSaveStateMiddleware(conversationState))

//Recognizers
const foneticRecognizer = new FoneticRecognizer()

// Create the main dialog.
const bot = new SchedulerBot(conversationState, foneticRecognizer)

// Listen for incoming requests.
server.post('/api/messages', (req, res) => {
    adapter.processActivity(req, res, async (context) => {
        // Route to main dialog.
        await bot.run(context);
    });
});

// Listen for Upgrade requests for Streaming.
server.on('upgrade', (req, socket, head) => {
    // Create an adapter scoped to this WebSocket connection to allow storing session data.
    const streamingAdapter = new BotFrameworkAdapter({
        appId: process.env.MicrosoftAppId,
        appPassword: process.env.MicrosoftAppPassword
    });
    // Set onTurnError for the BotFrameworkAdapter created for each connection.
    streamingAdapter.onTurnError = onTurnErrorHandler;

    streamingAdapter.useWebSocket(req, socket, head, async (context) => {
        // After connecting via WebSocket, run this logic for every request sent over
        // the WebSocket connection.
        await bot.run(context);
    });
});
