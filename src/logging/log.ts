import * as pino from "pino";
import { config } from '../config/config'

const censor = "*****"
const redactRegex = /(--(?!-).+?--)/g //Match with everything between "--" and avoid matching with several "-----" in a row

const redact = (text: string): string => {
    return text.replace(redactRegex, censor)
}

//TODO: redact all args passed to logger
const redactArgs = (inputArgs: any) => {
    if (typeof inputArgs == 'string') {
        inputArgs = redact(inputArgs)
    }
    return inputArgs
}


const removeRedactedMarks = (text: string) => {
    let matches = text.match(redactRegex) || []
    for (const match of matches) {
        let newString = match;
        newString = newString.replace(/^--/g, "")
        newString = newString.replace(/--$/g, "")
        text = text.replace(match, newString)
    }
    return text
}

//TODO: remove redaction marks on all args passed to logger
const removeRedactedMarksArgs = (inputArgs: any) => {
    if (typeof inputArgs == 'string') {
        inputArgs = removeRedactedMarks(inputArgs)
    }
    return inputArgs
}

let options = {
    level: config.app.loglevel,
    base: null,
    prettyPrint: {
        translateTime: 'yyyy-mm-dd HH:MM:ss.l',
        hideObject: true
    },
    formatters: {
        level: (label) => {
            return { level: label };
        }
    }
}

let logger = pino(options);

//Global redaction control
let isLoggerRedactable = false

//Wrapper for logger functions that preprocesses the message to redact it if necessary
export const log = {

    shouldRedactLogs: (value) => {
        isLoggerRedactable = value
    },

    trace: (inputArgs) => {
        logger.trace(inputArgs)
    },
    debug: (inputArgs) => {
        logger.debug(inputArgs)
    },
    info: (inputArgs) => {
        logger.info(inputArgs)
    },
    warn: (inputArgs) => {
        logger.warn(inputArgs)
    },
    error: (inputArgs) => {
        logger.error(inputArgs)
    },
    fatal: (inputArgs) => {
        logger.fatal(inputArgs)
    },
    silent: (inputArgs) => {
        logger.silent(inputArgs)
    },
    redact: {
        trace: (inputArgs) => {
            if (!isLoggerRedactable) return logger.trace(removeRedactedMarksArgs(inputArgs))
            logger.trace(redactArgs(inputArgs))
        },
        debug: (inputArgs) => {
            if (!isLoggerRedactable) return logger.debug(removeRedactedMarksArgs(inputArgs))
            logger.debug(redactArgs(inputArgs))
        },
        info: (inputArgs) => {
            if (!isLoggerRedactable) return logger.info(removeRedactedMarksArgs(inputArgs))
            logger.info(redactArgs(inputArgs))
        },
        warn: (inputArgs) => {
            if (!isLoggerRedactable) return logger.warn(removeRedactedMarksArgs(inputArgs))
            logger.warn(redactArgs(inputArgs))
        },
        error: (inputArgs) => {
            if (!isLoggerRedactable) return logger.error(removeRedactedMarksArgs(inputArgs))
            logger.error(redactArgs(inputArgs))
        },
        fatal: (inputArgs) => {
            if (!isLoggerRedactable) return logger.fatal(removeRedactedMarksArgs(inputArgs))
            logger.fatal(redactArgs(inputArgs))
        },
        silent: (inputArgs) => {
            if (!isLoggerRedactable) return logger.silent(removeRedactedMarksArgs(inputArgs))
            logger.silent(redactArgs(inputArgs))
        }
    }
}


