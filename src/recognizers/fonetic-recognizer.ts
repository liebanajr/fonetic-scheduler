const axios = require('axios');
import { log } from "../logging/log";
import { config } from '../config/config';

export class FoneticRecognizer {

    private url: string;

    constructor() {
        //TODO: sacar a configuración la URL
        this.url = config.app.foneticRecognizer.endpoint
    }

    public async recognize(literal: string, recognizer: string, converId: string, channel: string, grammar?: string, disambiguations?: object, minimDigit?:number, maxDigit?:number): Promise<RecognitionResponse> {
        try {
            if (literal == "noinput") {
                return {
                    "literal": "silencio",
                    "top_result": {
                        "result": "noinput"
                    }
                }
            }
            const response = await axios.get(this.url, {
                "params": {
                    "literal": literal,
                    "recognizer": recognizer,
                    "grammar": grammar,
                    "disambiguations": disambiguations,
                    "minim": minimDigit,
                    "maxim": maxDigit
                }
            });
            return response.data;
        } catch (err) {
            log.error(`ID: ${converId} | Channel: ${channel} | ${this.constructor.name.toString()} | Error recognizing: ${err.message}`);
            return null;
        }
    }


    public static getTopResult(response: RecognitionResponse): string {
        return response.top_result.result;
    }
}

export class RecognitionResponse {
    public literal: string;
    public altered_literal?: string;
    public top_result: RecognitionResult;
    public results?: RecognitionResult[];
}

export class RecognitionResult {
    public result: string;
    public confidence?: number;
}